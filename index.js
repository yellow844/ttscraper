"use strict";
var http = require("http"), FeedParser = require("feedparser"), client = require("cheerio-httpcli");
module.exports = (function(){

  var
    offcial_source = "http://columbia.jp/trustrick/",
    path = {
    official: {
      whatsNew: offcial_source
    }
  };

  function _fetch_data(url, callback){
    return new Promise(function(resolve){
      client.fetch(url, function (err, $, res) {
        resolve(callback.call(null, $, res));
      });
    });
  }

  function _parse_rss(path){
    return new Promise(function(resolve){
      http.get( path, function(res){
        var array = [], _meta;
        res.pipe(new FeedParser({})).on('meta', function(meta) {
          _meta = meta;
        }).on('readable', function() {
          var _this = this, item;
          while (item = _this.read()) {
//            console.log(item);
            array.push({
              title: item.title,
              link: item.link,
              pubDate: item.pubDate,
              description: item.description
            });
          }
        }).on('end', function() {
          resolve({
            meta:_meta,
            items:array
          });
        })
      });
    })
  }

  var TTScraper = {
    official:{
      whatsNew: (function(){
       return _fetch_data(path.official.whatsNew, function($, res){
          var arr = [], $WhatsNew = $("#WhatsNew dl"), dt = $WhatsNew.find("dt"), dd = $WhatsNew.find("dd"), max, i;
          max = (dt.length >= dd.length)? dd.length : dt.length;
          for(let i = 0; i < max ; i++) {
            let $dd = $(dd[i]);
            arr[i] = {
              date: $(dt[i]).text(),
              path: offcial_source + $dd.find("a").attr("href"),
              text: $dd.text()
            }
          }
          return arr;
        });
      })()
    },
    blog:{
      sayaka:(function(){
        return _parse_rss("http://feedblog.ameba.jp/rss/ameblo/sayaka-kanda/rss20.xml");
      })(),
      billy:(function(){
        return _parse_rss("http://feedblog.ameba.jp/rss/ameblo/blystudio/rss20.xml");
      })()
    }
  };
  return Object.create(TTScraper);
})();